-- Выборка обращений по оценок и скорости закрытия
select 
flat_table.mark_code,
sum(case when flat_table.minutes_reaction<=720 then 1 else 0 end ) as fast_reactions,
sum(case when flat_table.minutes_reaction>720 then 1 else 0 end ) as low_reactions

from 
    (
        select 
        user_id,
        ticket_subcategory, 
        request_id,
        fact_reaction_dt,
        minutes_reaction,
        max_dt_solved_tickets,
        case when mark like 'Не удовлетворительно' then 1
             when mark like 'Удовлетворительно' then 2
             when mark like 'Нейтрально' then 3
             when mark like 'Хорошо' then 4
             when mark like 'Отлично' then 5
             else 0
        end as mark_code,
 --- выберем кол-во новых объявлений после последнего обращения в поодержку       
(select count(distinct item_id) from new_items_by_support_users where user_id =marks.user_id and item_starttime>marks.max_dt_solved_tickets ) as count_new_items_after_last_ticket 

        from 
            (
            select 
            tickets.user_id,
            tickets.ticket_subcategory,
            tickets.request_id,
            tickets.fact_reaction_dt,
            EXTRACT(EPOCH from (fact_reaction_dt - activity_start_dt))/60 as minutes_reaction,
            (select result_mentioned_by_user from users_evaluation_of_satisfaction as marks where marks.request_id = tickets.request_id group by 1  ) as mark,
            (select max(fact_reaction_dt) from public.support_tickets where user_id = tickets.user_id ) as max_dt_solved_tickets
            from public.support_tickets  as tickets 

            ) as marks

    ) as flat_table
    
where flat_table.mark_code>0

group by 1


