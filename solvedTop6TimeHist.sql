-- оценка скорости решения обращений по 6 наиболее популярным запросам

select
	width_bucket(minustes_reaction::INT, 720, 72000, 99 ) as solved_time_hist, -- hist with 100 intervals by 12 hours
	count(request_id) as tickets
	from (
			select
				user_id,
				ticket_subcategory,
				request_id,
				EXTRACT(EPOCH from (fact_reaction_dt - activity_start_dt))/60 as minustes_reaction
				from public.support_tickets
			 ) as tickets
					where ticket_subcategory in
					(
							select
							ticket_subcategory
							from (
								select
								ticket_subcategory,
								count(request_id)
								from public.support_tickets
								group by 1 order by 2 desc
									 ) as popular
								limit 6
					)

	group by 1
	order by 1
