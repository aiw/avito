-- Оценка скорости решений обращений по 18 часовым интервалам

select
	width_bucket(minustes_reaction::INT, 1080, 108000, 99 ) as solved_time_hist, -- hist with 100 intervals by 18 hours
	count(request_id) as tickets
	from (
			select
				user_id,
				ticket_subcategory,
				request_id,
				EXTRACT(EPOCH from (fact_reaction_dt - activity_start_dt))/60 as minustes_reaction
				from public.support_tickets
			 ) as tickets

	group by 1
	order by 1
