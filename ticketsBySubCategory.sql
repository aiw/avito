-- выборка запросов в поддержку

select 
ticket_subcategory,
count_users,
count_tickets,
total_users,
total_tickets,
round(100.0*count_users/total_users, 2) as percent_of_all_users,
round(100.0*count_tickets/total_tickets, 2) as percent_of_all_tickets

from 
	(
		select
		ticket_subcategory,
		count(distinct user_id) as count_users,
        count(distinct request_id) as count_tickets,
        (select count(distinct user_id) from public.support_tickets ) as total_users,
        (select count(distinct request_id) from public.support_tickets ) as total_tickets

         from public.support_tickets 
		 group by 1

) as tickets
order by 2 desc

